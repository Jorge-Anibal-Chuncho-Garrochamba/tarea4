# Capitulo 5
# Ejercicio 1
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec

# Escribe un programa que lea repetidamente números hasta que el usuario introduzca “ﬁn”. Una vez se haya introducido “ﬁn”, muestra por pantalla el total, la cantidad de números y la media de esos números. Si el usuario introduce cualquier otra cosa que no sea un número, detecta su fallo usando try y except, muestra un mensaje de error y pasa al número siguiente.

Lista = 0
Total = 0
while True:
    num = input("Introduzca un número: ")
    try:
        Total = Total + int(num)
        Lista = Lista + 1
    except:
        if num in "fin":
            break
        print ("Entrada Invalida")

print(Total, Lista, (Total/Lista))