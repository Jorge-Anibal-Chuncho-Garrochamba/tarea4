# Cpitulo 5
# Ejercicio 2
# Autor = "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec
# Escribe otro programa que pida una lista de números como la anterior y al ﬁnal muestre por pantalla el máximo y mínimo de los números, en vez de la media.

lista = []
cantidad = int(input("Ingrese la Cantidad de lista deseada: "))
# Cantidad es el número para crear la lista las veces deseadas
Mayor = 0
Menor = 0
i = 1
while(cantidad > 0):
    numero = float(input("numero #" + str(i) + ": "))
    lista.append(numero)
    i = i + 1
    cantidad = cantidad - 1
Mayor = max(lista)
Menor = min(lista)

print("Lista: ", lista)
print("Mayor: ", Mayor)
print("Menor: ", Menor)